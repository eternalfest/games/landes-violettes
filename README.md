# Landes Violettes

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/landes-violettes.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/landes-violettes.git
```
