import external.*

class external.mods.Debug extends external.core.ExternalModule {

  public var ModInterface = {};
  public var ModDatas = {};

  function init() {
    if (external.core.Main.IS_DEBUG) {
      EL.log('DEBUG : OK');
      initStuff();
    }
  }

  function initStuff() {
    var self = this;

    var AbstractGameMode = Game.mode.GameMode;
    var PlayerEffects = Game.PlayerEffects;
    var Data = Game.Data;

    EL.onMethodCall(AbstractGameMode, "main", null, function () {
      this.curMan.isDebug = true;
      //if (this.isGamePaused) this.printScriptsDebugInfo();
    });

    EL.replaceMethod(AbstractGameMode, "processKeyEvents", function (old) {
      if (!this.effects) {
        this.effects = new PlayerEffects(this, this);
      }
      if (Key.isDown(97) && this.lastKeyPressed != 97) {
        this.effects.effectLevelWarp(1);
        this.lastKeyPressed = 97;
      }
      if (Key.isDown(98) && this.lastKeyPressed != 98) {
        this.effects.effectLevelWarp(2);
        this.lastKeyPressed = 98;
      }
      if (Key.isDown(99) && this.lastKeyPressed != 99) {
        this.effects.effectLevelWarp(3);
        this.lastKeyPressed = 99;
      }
      if (Key.isDown(100) && this.lastKeyPressed != 100) {
        this.effects.effectLevelWarp(4);
        this.lastKeyPressed = 100;
      }
      if (Key.isDown(101) && this.lastKeyPressed != 101) {
        this.effects.effectLevelWarp(5);
        this.lastKeyPressed = 101;
      }
      if (Key.isDown(109) && this.lastKeyPressed != 109) {
        this.effects.effectLevelWarp(-1);
        this.lastKeyPressed = 109;
      }
      if (Key.isDown(107) && this.lastKeyPressed != 107) {
        this.effects.effectLevelWarp(1);
        this.lastKeyPressed = 107;
      }
      if (Key.isDown(106) && this.lastKeyPressed != 106) {
        this.effects.effectLevelWarp(10);
        this.lastKeyPressed = 106;

      }
      if (Key.isDown(69) && this.lastKeyPressed != 69) {
        var bad = this.getFruits();
        for (var i = 0; i < bad.length; i++) {
          bad[i].removeSprite();
        }
        this.lastKeyPressed = 69;
      }
      old();
    });

    EL.onMethodCall(Game.entities.Player, "killAndRespawn", function () {
      if (this.lives <= 0)
        this.lives = 1;
    });

    //initSpriteIndicators();

  }

  /*function initSpriteIndicators() {
    var datas = ref.gDatas;
    EL.setMethod(ref.gMode.AbstractGameMode, "showIndicators", function(show){
      this.listDebugIndicator = null;
      this.indicCount = 0;
      this.indicatorScene.removeMovieClip();

      if(show) {
        this.listDebugIndicator = [];
        this.indicatorScene = _global.lib.addEmptyMovie(this.rootClip, this["+(WHk"].beastNumber++);
        this.indicatorScene._x = this.gameXOffset;
        this.indicatorScene._y = this.gameYOffset;
      }

      this.isIndicatorVisible = show;
    });

    EL.onMethodCall(ref.gMode.AbstractGameMode, "onNewFrame", null, function(){
      if(this.isIndicatorVisible)
        this.updateIndicators();
    });

    EL.onMethodCall(ref.gMode.AbstractGameMode, "processKeyEvents", null, function(){
      if (Key.isDown(88) && this.lastKeyPressed != 88) {
            this.showIndicators(!this.isIndicatorVisible);
            this.lastKeyPressed = 88;
      }
    });

    EL.setMethod(ref.gMode.AbstractGameMode, "updateIndicators", function(){
      var sprites = this.getSprites(datas.type_all);

      if(this.listDebugIndicator.length > sprites.length){
        for(var i = this.listDebugIndicator.length-1; i >= sprites.length; i--)
          this.listDebugIndicator[i].removeMovieClip();

        this.listDebugIndicator.splice(sprites.length);
      } else if(this.listDebugIndicator.length < sprites.length){
        for(var i = this.listDebugIndicator.length; i < sprites.length; i++){
          var indic = this.indicatorScene.createEmptyMovieClip("indicator#" + this.indicCount++, this.indicatorScene.getNextHighestDepth());
          indic.lineStyle(1, 0xff0000, 100);
          indic.moveTo(-5, 0);
          indic.lineTo(5, 0);
          indic.moveTo(0, -5);
          indic.lineTo(0, 5);
          this.listDebugIndicator.push(indic);
        }
      }

      for(var i = sprites.length-1; i >= 0; i--){
        var indic = this.listDebugIndicator[i];
        var sprite = sprites[i];
        indic._x = sprite.posX;
        indic._y = sprite.posY;
      }

    });
  }*/

}
