import external.*

class external.mods.FixTilesAI extends external.core.ExternalModule {

  public var ModInterface = {};
  public var ModDatas = {};

  public function init() {
    var Data = Game.Data;

    EL.require("scripts.BetterScript").registerEvent("e_recalculateAI", function(event) {
      var y = this.getParam(event, 'y');
      var y1 = y == null ? this.getParam(event, 'y1') : y;
      var y2 = y == null ? this.getParam(event, 'y2') : y;

      if(y1 > y2) {
        var tmp = y1;
        y1 = y2;
        y2 = tmp;
      }
      y2++;
      
      var world = this.game.world;
      var old = Data.TILE_IA_PASSES;
      Data.TILE_IA_PASSES = (y2 - y1) * Data.LEVEL_CWIDTH;
      world.curMan.progress = null;
      world.onLevelLoaded = null;

      for(y = y1; y < y2; y++) {
        for(var x = 0; x < Data.LEVEL_CWIDTH; x++) {
          this.tilesFlags[x][y] = 0;
          this.landingHeightMap[x][y] = -1;
        }
      }
      world.computeFlags({ cx: 0, cy: y1 });

      delete world.curMan.progress;
      delete world.onLevelLoaded;
      Data.TILE_IA_PASSES = old;
    });
  }
}